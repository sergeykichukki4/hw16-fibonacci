"use strict";

let num;

do {
    num = prompt("Enter number");
} while (!num || isNaN(num) || !Number.isInteger(+num) || +num < 0)

function fibonacci (f0, f1, n) {
    debugger
    if (n === 1) {
        return f0;
    }
    if (n === 2) {
        return f1;
    }

    return fibonacci(f0, f1, n - 1) + fibonacci(f0, f1, n - 2);
}

console.log(fibonacci(13,15, num));
